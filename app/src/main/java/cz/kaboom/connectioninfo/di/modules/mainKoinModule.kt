package cz.kaboom.connectioninfo.di.modules

import cz.kaboom.connectioninfo.providers.InternetStateProvider
import cz.kaboom.connectioninfo.providers.NetworkInfoProvider
import cz.kaboom.connectioninfo.providers.NetworkSpeedProvider
import cz.kaboom.connectioninfo.common.Const
import cz.kaboom.connectioninfo.common.FloatCalcField
import cz.kaboom.connectioninfo.viewmodel.MainActivityViewModel
import fr.bmartel.speedtest.SpeedTestSocket
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

val mainKoinModule = module {
    viewModel { MainActivityViewModel(get(),get(),get()) }
    factory { NetworkInfoProvider(get(), get { parametersOf(Const.IPAPI_BASE_URL) }, get { parametersOf(Const.LOOKUP_BASE_URL) }) }
    factory { NetworkSpeedProvider(get(), get()) }
    factory { InternetStateProvider(get()) }
    factory { FloatCalcField() }
    single { SpeedTestSocket() }
    factory { (baseUrl: String) ->
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build() ?: throw Exception("Cannot create Retrofit service for URL $baseUrl")
    }
}

