package cz.kaboom.connectioninfo.common

class FloatCalcField{
    val current: String get() = formatString.format(mCurrent)
    val maximum: String get() = formatString.format(mMaxValue)
    val minimum: String get() = formatString.format(mMinValue)
    val average: String get() = formatString.format(mAccumulator / mCounter)

    private var mCurrent: Float = 0f
    private var mMaxValue: Float = 0f
    private var mMinValue: Float = 0f
    private var mAccumulator: Float = 0f
    private var mCounter: Int = 0
    var formatString: String = "%.2f"

    fun reset() {
        mCurrent = 0f
        mMaxValue = 0f
        mMinValue = 0f
        mAccumulator = 0f
        mCounter = 0
    }

    fun set(value: Float) {
        mCurrent = value
        mCounter++
        mAccumulator += value
        if (value > mMaxValue) mMaxValue = value
        if (value < mMinValue) mMinValue = value
    }

    fun get(): Float {
        return mCurrent
    }
}
